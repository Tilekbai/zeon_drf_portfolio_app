from rest_framework.response import (
    Response,
)
from rest_framework import (
    generics,
) 
from rest_framework.views import (
    APIView,
    )
from rest_framework import (
    permissions,
    )
from .models import (
    ToDoList,
    ToDoItem,
)
from .serializers import (
    ToDoListSerializer,
    ToDoItemSerializer,
)


class ListListView(generics.ListAPIView):
    queryset = ToDoList.objects.all()
    serializer_class = ToDoListSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        context = {'todolists': serializer.data}
        return Response(context)
    
class ListCreateView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, *args, **kwargs):
        serializer = ToDoListSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

class ListDeleteView(generics.DestroyAPIView):
    queryset = ToDoList.objects.all()
    serializer_class = ToDoListSerializer
    permission_classes = [permissions.IsAuthenticated]

class ItemCreateView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, list_id, format=None):
        data = request.data
        data['todo_list_id'] = list_id
        serializer = ToDoItemSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

class ItemDeleteView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def delete(self, request, pk, format=None):
        try:
            todo_item = ToDoItem.objects.get(pk=pk)
        except ToDoItem.DoesNotExist:
            return Response({'detail': 'ToDoItem not found.'}, status=404)

        todo_item.delete()
        return Response(status=204)

class ItemUpdateView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def put(self, request, pk, format=None):
        try:
            todo_item = ToDoItem.objects.get(pk=pk)
        except ToDoItem.DoesNotExist:
            return Response({'detail': 'ToDoItem not found.'}, status=404)

        serializer = ToDoItemSerializer(todo_item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

class ItemListView(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ToDoItemSerializer
    queryset = ToDoItem.objects.all()

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset().filter(todo_list=self.kwargs['list_id'])
        serializer = self.get_serializer(queryset, many=True)
        context = {'items': serializer.data}
        return Response(context)
