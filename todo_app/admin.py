from django.contrib import admin
from .models import (
    ToDoList, 
    ToDoItem,
)

class ToDoListAdmin(admin.ModelAdmin):
    pass

class ToDoItemAdmin(admin.ModelAdmin):
    pass

admin.site.register(ToDoList, ToDoListAdmin)
admin.site.register(ToDoItem, ToDoItemAdmin)