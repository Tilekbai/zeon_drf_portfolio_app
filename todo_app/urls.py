from django.urls import (
    path,
)
from .views import *


urlpatterns = [
    path('', ListListView.as_view(), name='index'),
    path("list/add/", ListCreateView.as_view(), name="list-add"),
    path("list/<int:pk>/delete/", ListDeleteView.as_view(), name="list-delete"),
    path('list/<int:list_id>/', ItemListView.as_view(), name='item-list'),
    path("list/<int:list_id>/item/add/", ItemCreateView.as_view(), name="item-add"),
    path("list/item/<int:pk>/", ItemUpdateView.as_view(), name="item-update"),
    path("list/item/<int:pk>/delete/", ItemDeleteView.as_view(), name="item-delete"),
]