from django.test import (
    TestCase, 
    RequestFactory,
)
from .models import (
    ToDoList,
    ToDoItem,
)
from rest_framework import status
from django.contrib.auth import get_user_model
from rest_framework.test import force_authenticate
from django.conf import settings
from .views import *


User = get_user_model()

class ListListViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ListListView.as_view()
        self.todolists = ToDoList.objects.create(title='ToDo List 1')

    def test_list_list_view(self):
        request = self.factory.get('api/todo_list/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['todolists']), 1)

    def test_list_list_view_empty(self):
        ToDoList.objects.all().delete()
        request = self.factory.get('api/todo_list/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['todolists']), 0)

class ListCreateViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ListCreateView.as_view()

    def test_list_create_view(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.post('api/todo_list/add/', {
            'title': 'Test Title'
        }, format='json')

        force_authenticate(request, user=user)
        response = self.view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['title'], 'Test Title')

    def test_list_create_view_invalid_data(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.post('api/todo_list/add/', {
            'title': '',
        }, format='json')

        force_authenticate(request, user=user)
        response = self.view(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list_create_view_unauthenticated(self):
        request = self.factory.post('api/todo_list/add/', {
            'title': 'Test Title'
        }, format='json')

        response = self.view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class ListDeleteViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ListDeleteView.as_view()
        self.list = ToDoList.objects.create(title='Test List')

    def test_list_delete_view(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.delete(f'api/todo_list/list/{self.list.pk}/delete/')
        force_authenticate(request, user=user)
        response = self.view(request, pk=self.list.pk)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(ToDoList.objects.filter(pk=self.list.pk).exists())

    def test_list_delete_view_not_found(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.delete('api/todo_list/list/999/delete/')
        force_authenticate(request, user=user)
        response = self.view(request, pk=999)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class ItemListViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ItemListView.as_view()
        self.todo_list = ToDoList.objects.create(
                title='Todo list 1'
            )
        self.todo_item = ToDoItem.objects.create(
            title='ToDo Item 1',
            description='Test description',
            todo_list=self.todo_list,
            )

    def test_item_list_view(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.get(f'api/todo_list/list/1/')
        force_authenticate(request, user=user)
        response = self.view(request, list_id=self.todo_list.pk)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['items']), 1)

    def test_item_list_view_empty(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        ToDoList.objects.all().delete()
        request = self.factory.get(f'api/todo_list/list/1/')
        force_authenticate(request, user=user)
        response = self.view(request, list_id=self.todo_list.pk)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['items']), 0)
    
class ItemCreateViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ItemCreateView.as_view()
        self.todo_list = ToDoList.objects.create(
                title='Todo list 1'
            )

    def test_item_create_view(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.post(f'api/todo_list/list/{self.todo_list.pk}/item/add/', {
            'title': 'Test Title',
            'description': 'Test Description',
            'todo_list': self.todo_list.pk
        }, format='json')

        request._mutable = True
        request.POST = request.POST.copy()

        force_authenticate(request, user=user)
        response = self.view(request, list_id=self.todo_list.pk)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['title'], 'Test Title')

    def test_item_create_view_invalid_data(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.post(f'api/todo_list/list/{self.todo_list.pk}/item/add/', {
            'title': '',
            'description': '',
            'todo_list': self.todo_list.pk
        }, format='json')

        request._mutable = True
        request.POST = request.POST.copy()

        force_authenticate(request, user=user)
        response = self.view(request, list_id=self.todo_list.pk)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list_create_view_unauthenticated(self):
        request = self.factory.post(f'api/todo_list/list/{self.todo_list.pk}/item/add/', {
            'title': 'Test Title'
        }, format='json')

        response = self.view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class ItemDeleteViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ItemDeleteView.as_view()
        self.todo_list = ToDoList.objects.create(
                title='Todo list 1'
            )
        self.item = ToDoItem.objects.create(
            title='ToDo Item 1',
            description='Test description',
            todo_list=self.todo_list,
            )

    def test_item_delete_view(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.delete(f'api/todo_list/list/item/{self.todo_list.pk}/delete/')
        force_authenticate(request, user=user)
        response = self.view(request, pk=self.item.pk)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(ToDoItem.objects.filter(pk=self.item.pk).exists())

    def test_item_delete_view_not_found(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.delete('api/todo_list/list/item/999/delete/')
        force_authenticate(request, user=user)
        response = self.view(request, pk=999)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_item_delete_view_notauthenticated(self):
        request = self.factory.delete('api/todo_list/list/item/999/delete/')
        response = self.view(request, pk=999)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class ItemUpdateViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ItemUpdateView.as_view()
        self.todo_list = ToDoList.objects.create(title='Test List')
        self.todo_item = ToDoItem.objects.create(title='Test Item', todo_list=self.todo_list)

    def test_item_update_view(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.put(f'/api/todo_list/list/item/{self.todo_item.pk}/', {
            'title': 'Updated Title',
            'description': 'Updated Description',
            'due_date': '2023-07-14T11:31:38.767925Z',
            'todo_list_id': self.todo_list.pk,
        }, content_type='application/json')

        force_authenticate(request, user=user)
        response = self.view(request, pk=self.todo_item.pk)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], 'Updated Title')
        self.assertEqual(response.data['description'], 'Updated Description')
        self.assertEqual(response.data['due_date'], '2023-07-14T11:31:38.767925Z')
