from rest_framework import (
    serializers,
)
from .models import (
    ToDoItem,
    ToDoList,
)


class ToDoListSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(max_length=100)

    def create(self, validated_data):
        return ToDoList.objects.create(**validated_data)

class ToDoItemSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(max_length=100)
    description = serializers.CharField(allow_blank=True, required=False)
    created_date = serializers.DateTimeField(read_only=True)
    due_date = serializers.DateTimeField(required=False)
    todo_list_id = serializers.PrimaryKeyRelatedField(
        queryset=ToDoList.objects.all(),
        source='todo_list',
        write_only=True
    )

    def create(self, validated_data):
        return ToDoItem.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.description)
        instance.due_date = validated_data.get('due_date', instance.due_date)
        instance.save()
        return instance

