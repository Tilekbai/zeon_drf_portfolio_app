from rest_framework import (
    serializers,
)
from .models import (
    Project,
)


class ProjectSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(required=False, max_length=100)
    description = serializers.CharField(required=False, style={'base_template': 'textarea.html'})
    technology = serializers.CharField(required=False, max_length=20)
    image = serializers.FilePathField(path="projects/static/img")