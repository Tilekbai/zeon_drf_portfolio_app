from rest_framework.response import (
    Response,
)
from rest_framework import (
    generics,
)
from rest_framework.renderers import (
    TemplateHTMLRenderer,
)
from .models import (
    Project,
)
from .serializers import (
    ProjectSerializer,
)


class ProjectIndexView(generics.ListAPIView):
    queryset = Project.objects.all().order_by('id')
    serializer_class = ProjectSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        context = {'projects': serializer.data}
        return Response(context)

class ProjectDetailView(generics.RetrieveAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    lookup_field = 'pk'

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        context = {'project': serializer.data}
        return Response(context)
    