from django.test import (
    TestCase, 
    RequestFactory,
)
from django.conf import settings
from .models import (
    Project,
)
from .views import (
    ProjectIndexView,
    ProjectDetailView,
)


class ProjectIndexViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ProjectIndexView.as_view()
        self.projects = [
            Project.objects.create(
                title='Project 1',
                description='Django',
                technology='DRF',
                image='projects/static/img/project1.png'
            ),
            Project.objects.create(
                title='Project 1',
                description='Django',
                technology='DRF',
                image='projects/static/img/project1.png'
            )
        ]

    def test_project_index_view(self):
        request = self.factory.get('/api/projects/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['projects']), len(self.projects))

    def test_project_index_view_empty(self):
        Project.objects.all().delete()
        request = self.factory.get('/api/projects/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['projects']), 0)

class ProjectDetailViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = ProjectDetailView.as_view()
        self.project = Project.objects.create(
            title='Project 1',
            description='Django',
            technology='DRF',
            image='projects/static/img/project1.png'
        )

    def test_project_detail_view(self):
        request = self.factory.get(f'/api/projects/{self.project.pk}/')
        response = self.view(request, pk=self.project.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['project']['id'], self.project.id)
        self.assertEqual(response.data['project']['title'], self.project.title)
        self.assertEqual(response.data['project']['description'], self.project.description)
        self.assertEqual(response.data['project']['technology'], self.project.technology)
        self.assertEqual(response.data['project']['image'], self.project.image)

    def test_project_detail_view_not_found(self):
        request = self.factory.get('/api/projects/999/')
        response = self.view(request, pk=999)
        self.assertEqual(response.status_code, 404)