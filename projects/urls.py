from django.urls import (
    include, 
    path,
)
from django.conf import settings
from django.conf.urls.static import static
from projects.views import *


urlpatterns = [
    path('', ProjectIndexView.as_view(), name='project_index'),
    path('<int:pk>/', ProjectDetailView.as_view(), name='project_detail'),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)