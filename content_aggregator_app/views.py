from rest_framework.response import (
    Response,
)
from rest_framework import (
    generics,
)
from rest_framework import (
    permissions,
    )
from urllib.parse import unquote
from .serializers import (
    ReleaseSerializer,
    NewsBlogSerializer,
)
from .models import (
    Release,
    NewsBlog,
)


class AllPodcastView(generics.ListAPIView):
    queryset = Release.objects.all().filter().order_by("-pub_date")[:20]
    serializer_class = ReleaseSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        context = {'podcasts': serializer.data}
        return Response(context)

class EveryPodcastView(generics.RetrieveAPIView):
    queryset = Release.objects.all()
    serializer_class = ReleaseSerializer

    def get(self, request, *args, **kwargs):
        podcast_name = self.kwargs['podcast_name']
        queryset = self.get_queryset().filter(podcast_name=podcast_name).order_by('-pub_date')[:10]
        serializer = self.get_serializer(queryset, many=True)
        context = {'podcasts': serializer.data}
        return Response(context)
    
class NewsBlogList(generics.ListAPIView):
    queryset = NewsBlog.objects.all()
    serializer_class = NewsBlogSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset().filter().order_by("-pub_date")[:10]
        serializer = self.get_serializer(queryset, many=True)
        context = {'news': serializer.data}
        return Response(context)
        
class EveryNewsBlogView(generics.RetrieveAPIView):
    queryset = NewsBlog.objects.all()
    serializer_class = NewsBlogSerializer

    def get(self, request, *args, **kwargs):
        section = self.kwargs['section']
        queryset = self.get_queryset().filter(section=section).order_by('-pub_date')[:10]
        serializer = self.get_serializer(queryset, many=True)
        context = {'news': serializer.data}
        return Response(context)
