from django.contrib import admin

from .models import Release, NewsBlog


@admin.register(Release)
class ReleaseAdmin(admin.ModelAdmin):
    list_display = ("podcast_name", "title", "pub_date")

@admin.register(NewsBlog)
class NewsBlogAdmin(admin.ModelAdmin):
    list_display = ("section", "title", "pub_date")