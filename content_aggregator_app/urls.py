from django.urls import path, include

from .views import *

urlpatterns = [
    path("", AllPodcastView.as_view(), name="all_podcast"),
    path("podcast/<str:podcast_name>/", EveryPodcastView.as_view(), name="every-podcast-list"),
    path("newsblog/", NewsBlogList.as_view(), name="news-blog-list"),
    path("news/<str:section>/", EveryNewsBlogView.as_view(), name="every-news-blog"),
]