from rest_framework import (
    serializers,
)


class ReleaseSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(max_length=200)
    description = serializers.CharField(style={'base_template': 'textarea.html'})
    pub_date = serializers.DateTimeField(read_only=True)
    link = serializers.URLField()
    image = serializers.URLField()
    podcast_name = serializers.CharField(max_length=100)
    guid = serializers.CharField(max_length=50)

class NewsBlogSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=200)
    description = serializers.CharField(style={'base_template': 'textarea.html'})
    pub_date = serializers.DateTimeField()
    link = serializers.URLField()
    section = serializers.CharField(max_length=100)
    guid = serializers.CharField(max_length=50)
    