from django.apps import AppConfig


class ContentAggregatorAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'content_aggregator_app'
