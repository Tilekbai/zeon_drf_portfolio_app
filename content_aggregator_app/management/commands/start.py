import logging

# Django
from django.conf import settings
from django.core.management.base import BaseCommand

# Third Party
import feedparser
from dateutil import parser
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.cron import CronTrigger
from django_apscheduler.jobstores import DjangoJobStore
from django_apscheduler.models import DjangoJobExecution

from content_aggregator_app.models import (
    Release, 
    NewsBlog,
)


logger = logging.getLogger(__name__)

def save_news_blog(feed):
    section_title = feed.channel.title

    for item in feed.entries:
        if not NewsBlog.objects.filter(guid=item.guid).exists():
            release = NewsBlog(
                title=item.title,
                description=item.description,
                pub_date=parser.parse(item.published),
                link=item.link,
                section=section_title,
                guid=item.guid,
            )
            release.save()
            
def save_new_releases(feed):
    podcast_title = feed.channel.title
    podcast_image = feed.channel.image

    for item in feed.entries:
        if not Release.objects.filter(guid=item.guid).exists():
            release = Release(
                title=item.title,
                description=item.description,
                pub_date=parser.parse(item.published),
                link=item.link,
                image=podcast_image,
                podcast_name=podcast_title,
                guid=item.guid,
            )
            release.save()

def fetch_knews_releases():
    _feed = feedparser.parse("https://www.knews.kg/feed")
    save_news_blog(_feed)

def fetch_mma_fighting_releases():
    _feed = feedparser.parse("https://www.mmafighting.com/rss/current.xml")
    save_news_blog(_feed)

def fetch_sportsru_releases():
    _feed = feedparser.parse("https://www.sports.ru/rss/topnews.xml")
    save_news_blog(_feed)

def fetch_redit_releases():
    _feed = feedparser.parse("https://www.reddit.com/r/podcasts.rss")   
    _feed.channel.image = _feed.channel.icon
    save_new_releases(_feed)

def fetch_lifehucker_relieses():
    _feed = feedparser.parse("https://lifehacker.ru/topics/podcast/feed/")
    save_new_releases(_feed)

def fetch_realpython_episodes():
    _feed = feedparser.parse("https://realpython.com/podcasts/rpp/feed")
    _feed.channel.image = _feed.channel.image["href"]
    save_new_releases(_feed)

def delete_old_job_executions(max_age=604_800):
    """Deletes all apscheduler job execution logs older than `max_age`."""
    DjangoJobExecution.objects.delete_old_job_executions(max_age)

class Command(BaseCommand):
    help = "Runs apscheduler."

    def handle(self, *args, **options):
        scheduler = BlockingScheduler(timezone=settings.TIME_ZONE)
        scheduler.add_jobstore(DjangoJobStore(), "default")
        

        scheduler.add_job(
            fetch_knews_releases,
            trigger="interval",
            minutes=2,
            id="K-News",
            max_instances=1,
            replace_existing=True,
        )
        logger.info("Added job: K-News.")

        scheduler.add_job(
            fetch_mma_fighting_releases,
            trigger="interval",
            minutes=2,
            id="MMA Fighting -  All Posts",
            max_instances=1,
            replace_existing=True,
        )
        logger.info("Added job: MMA Fighting -  All Posts.")

        scheduler.add_job(
            fetch_sportsru_releases,
            trigger="interval",
            minutes=2,
            id="Главные новости - Спорт: футбол, хоккей, баскетбол, теннис, бокс, Формула-1 – все новости спорта на Sports.ru",
            max_instances=1,
            replace_existing=True,
        )
        logger.info("Added job: Главные новости - Спорт: футбол, хоккей, баскетбол, теннис, бокс, Формула-1 – все новости спорта на Sports.ru.")

        scheduler.add_job(
            fetch_redit_releases,
            trigger="interval",
            minutes=2,
            id="Podcasts - discover, discuss, review",
            max_instances=1,
            replace_existing=True,
        )
        logger.info("Added job: Podcasts - discover, discuss, review.")

        scheduler.add_job(
            fetch_lifehucker_relieses,
            trigger="interval",
            minutes=2,
            id="Подкасты - Лайфхакер",
            max_instances=1,
            replace_existing=True,
        )
        logger.info("Added job: Подкасты - Лайфхакер.")

        scheduler.add_job(
            fetch_realpython_episodes,
            trigger="interval",
            minutes=2,
            id="The Real Python Podcast",
            max_instances=1,
            replace_existing=True,
        )
        logger.info("Added job: The Real Python Podcast.")

        scheduler.add_job(
            delete_old_job_executions,
            trigger=CronTrigger(
                day_of_week="mon", hour="00", minute="00"
            ),
            id="Delete Old Job Executions",
            max_instances=1,
            replace_existing=True,
        )
        logger.info("Added weekly job: Delete Old Job Executions.")

        try:
            logger.info("Starting scheduler...")
            scheduler.start()
        except KeyboardInterrupt:
            logger.info("Stopping scheduler...")
            scheduler.shutdown()
            logger.info("Scheduler shut down successfully!")