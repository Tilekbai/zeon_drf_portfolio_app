from django.test import (
    TestCase, 
    RequestFactory,
)
from .models import (
    Release,
    NewsBlog,
)
from .views import *


class AllPodcastViewTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.view = AllPodcastView.as_view()
        self.release = Release.objects.create(
            title='Test title', 
            description='Test description',
            pub_date='2023-07-14T11:31:38.767925Z', 
            link='http://test_link.com/test/',
            image='/image', 
            podcast_name='Test name',
            guid='Test guid',
            )

    def test_all_podcast_list_view(self):
        request = self.factory.get('api/content/aggregator/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['podcasts']), 1)

    def test_all_podcast_list_view_empty(self):
        Release.objects.all().delete()
        request = self.factory.get('api/content/aggregator/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['podcasts']), 0)

class EveryPodcastViewTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.view = EveryPodcastView.as_view()
        self.releases = [
            (Release.objects.create(
            title='Test title 1', 
            description='Test description 1',
            pub_date='2023-07-14T11:31:38.767925Z', 
            link='http://test_link.com/test/',
            image='/image', 
            podcast_name='Test name 1',
            guid='Test guid',
            )),
            (Release.objects.create(
            title='Test title 2', 
            description='Test description 2',
            pub_date='2023-07-14T11:31:38.767925Z', 
            link='http://test_link.com/test/23232323/',
            image='/image', 
            podcast_name='Test name 2',
            guid='Test guid',
            )),
        ]

    def test_every_podcast_list_view(self):
        request = self.factory.get(f'api/content/aggregator/podcast/{self.releases[0].podcast_name}/')
        response = self.view(request, podcast_name=self.releases[0].podcast_name)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['podcasts']), 1)

    def test_every_podcast_list_view_empty(self):
        Release.objects.all().delete()
        request = self.factory.get(f'api/content/aggregator/podcast/{self.releases[0].podcast_name}/')
        response = self.view(request, podcast_name=self.releases[0].podcast_name)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['podcasts']), 0)

class NewsBlogListTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.view = NewsBlogList.as_view()
        self.newsblog = NewsBlog.objects.create(
            title='Test title', 
            description='Test description',
            pub_date='2023-07-14T11:31:38.767925Z', 
            link='http://test_link.com/test/',
            section='Test section',
            guid='Test guid',
            )

    def test_newsblog_list_view(self):
        request = self.factory.get('api/content/aggregator/newsblog/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['news']), 1)

    def test_newsblog_list_view_empty(self):
        NewsBlog.objects.all().delete()
        request = self.factory.get('api/content/aggregator/newsblog/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['news']), 0)

class EveryNewsBlogViewTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.view = EveryNewsBlogView.as_view()
        self.news = [
            (NewsBlog.objects.create(
            title='Test title 1', 
            description='Test description 1',
            pub_date='2023-07-14T11:31:38.767925Z', 
            link='http://test_link.com/test/',
            section='Test section 1',
            guid='Test guid',
            )),
            (NewsBlog.objects.create(
            title='Test title 2', 
            description='Test description 2',
            pub_date='2023-07-14T11:31:38.767925Z', 
            link='http://test_link.com/test/23232323/',
            section='Test section 2',
            guid='Test guid',
            )),
        ]

    def test_every_newsblog_list_view(self):
        request = self.factory.get(f'api/content/aggregator/newsblog/{self.news[0].section}/')
        response = self.view(request, section=self.news[0].section)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['news']), 1)

    def test_every_newsblog_list_view_empty(self):
        NewsBlog.objects.all().delete()
        request = self.factory.get(f'api/content/aggregator/newsblog/{self.news[0].section}/')
        response = self.view(request, section=self.news[0].section)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['news']), 0)
