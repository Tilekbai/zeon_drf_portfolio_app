from django.contrib import (
    admin,
)
from django.urls import (
    include, 
    path,
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('users.urls', namespace='users')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/projects/', include(('projects.urls', 'projects'), namespace='projects')),
    path('api/blog/', include(('blog.urls', 'blog'), namespace='blog')),
    path('api/todo_list/', include(('todo_app.urls', 'todo_app'), namespace='todo_app')),
    path('api/diary/', include(('diary_app.urls', 'diary_app'), namespace='diary_app')),
    path('api/flashcards/', include(('cardapp.urls', 'cardapp'), namespace='cardapp')),
    path('api/content/aggregator/', include(('content_aggregator_app.urls', 'content_aggregator_app'), namespace='content_aggregator_app')),
]