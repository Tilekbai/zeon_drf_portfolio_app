from rest_framework import (
    serializers,
)
from .models import (
    Post,
    Comment,
    Category,
)


class CategorySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(max_length=20)

class PostSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(max_length=255)
    body = serializers.CharField(style={'base_template': 'textarea.html'})
    created_on = serializers.DateTimeField(read_only=True, format="%Y-%m-%d")
    last_modified = serializers.DateTimeField(read_only=True)
    categories = CategorySerializer(many=True)

class CommentSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    author = serializers.CharField(required=False)
    body = serializers.CharField(style={'base_template': 'textarea.html'})
    created_on = serializers.DateTimeField(read_only=True)
    post = serializers.SlugRelatedField(queryset=Post.objects.all(), slug_field='id')
    
    def create(self, validated_data):
        author = self.context['request'].user
        return Comment.objects.create(author=author, **validated_data)
