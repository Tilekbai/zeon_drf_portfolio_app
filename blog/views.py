from rest_framework.response import (
    Response,
)
from rest_framework import (
    generics,
) 
from rest_framework.views import (
    APIView,
    )
from rest_framework import (
    permissions,
    )
from .models import (
    Comment,
    Post,
)
from .serializers import (
    PostSerializer,
    CommentSerializer,
)


class CategoryPostView(generics.RetrieveAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset().filter(categories__name=self.kwargs['category'])
        serializer = self.get_serializer(queryset, many=True)
        context = {'posts': serializer.data}
        return Response(context)
    
class PostListView(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        context = {'posts': serializer.data}
        return Response(context)

class PostDetailView(APIView):
    permission_classes = [permissions.DjangoModelPermissionsOrAnonReadOnly]
    queryset = Post.objects.all()

    def get(self, request, *args, **kwargs):
        post = self.get_object()
        comments = Comment.objects.filter(post=post)
        post_serializer = PostSerializer(post)
        comments_serializer = CommentSerializer(comments, many=True)
        data = {
            'post': post_serializer.data,
            'comments': comments_serializer.data,
        }
        return Response(data)
    
    def get_object(self):
        id = self.kwargs.get('pk')
        post = Post.objects.get(id=id)
        return post

class CommentCreateView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, *args, **kwargs):
        serializer = CommentSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)