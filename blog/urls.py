from django.urls import (
    path,
)
from .views import *


urlpatterns = [
    path('', PostListView.as_view(), name='blog_index'),
    path('<category>/', CategoryPostView.as_view(), name='blog_category'),
    path('post/<int:pk>/', PostDetailView.as_view(), name='blog_detail'),
    path('post/<int:pk>/comment/create/', CommentCreateView.as_view(), name='comment_create'),
]