from django.test import (
    TestCase, 
    RequestFactory,
)
from .models import (
    Comment,
    Post,
    Category,
)
from rest_framework import status
from django.contrib.auth import get_user_model
from rest_framework.test import force_authenticate
from django.conf import settings
from .views import *
from django.core.exceptions import ObjectDoesNotExist


User = get_user_model()

class PostListViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = PostListView.as_view()
        self.category = Category.objects.create(name='Category 1')
        self.post = Post.objects.create(title='Post 1', body='Test post')
        self.post.categories.set([self.category])

    def test_post_list_view(self):
        request = self.factory.get('/api/blog/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['posts']), 1)
        self.assertEqual(response.data['posts'][0]['title'], self.post.title)
        self.assertEqual(response.data['posts'][0]['body'], self.post.body)
        self.assertEqual(response.data['posts'][0]['categories'][0]['name'], self.category.name)

    def test_post_list_view_empty(self):
        Post.objects.all().delete()
        request = self.factory.get('/api/blog/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['posts']), 0)

class PostDetailViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = PostDetailView.as_view()
        self.category = Category.objects.create(name='Category 1')
        self.post = Post.objects.create(
            title='Post 1',
            body='Test post'
        )
        self.comment = Comment.objects.create(
            author='Author 1',
            body='Test comment',
            post=self.post
        )

    def test_post_detail_view(self):
        request = self.factory.get(f'/api/blog/post/{self.post.pk}/')
        response = self.view(request, pk=self.post.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['post']['id'], self.post.id)
        self.assertEqual(response.data['post']['title'], self.post.title)
        self.assertEqual(response.data['post']['body'], self.post.body)
        self.assertEqual(response.data['comments'][0]['author'], self.comment.author)
        self.assertEqual(response.data['comments'][0]['body'], self.comment.body)

    def test_post_detail_view_not_found(self):
        invalid_id = 999
        request = self.factory.get(f'/api/blog/post/{invalid_id}/')
        
        with self.assertRaises(ObjectDoesNotExist):
            self.view(request, pk=invalid_id)

class CommentCreateViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = CommentCreateView.as_view()
        self.category = Category.objects.create(name='Category 1')
        self.post = Post.objects.create(
            title='Post 1',
            body='Test post'
        )
        self.comment = Comment.objects.create(
            author='Author 1',
            body='Test comment',
            post=self.post
        )

    def test_comment_create_view(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.post(f'api/blog/post/{self.post.pk}/comment/create/', {
            'body': 'Test Comment',
            'post': {self.post.pk}
        }, format='json')

        force_authenticate(request, user=user)
        response = self.view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['author'], user.username)
        self.assertEqual(response.data['body'], 'Test Comment')

    def test_comment_create_view_invalid_data(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.post('/api/blog/post/1/comment/create/', {
            'author': '',
            'body': '',
            'post': 999
            ,
        }, format='json')

        force_authenticate(request, user=user)
        response = self.view(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_comment_create_view_unauthenticated(self):
        request = self.factory.post('api/blog/post/1/comment/create/', {
            'author': 'Test Author',
            'body': 'Test Comment',
            'post': 1,
        }, format='json')

        response = self.view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
