from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.conf import settings
from django.contrib.auth import authenticate, get_user_model


User = get_user_model()

class UserRegistrationAPIViewTestCase(APITestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']

    def test_user_registration(self):
        url = reverse('users:api-register')
        data = {
            'username': 'testuser',
            'email': 'testuser@example.com',
            'password': 'testpassword'
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().username, 'testuser')

class UserLoginAPIViewTestCase(APITestCase):
    def test_user_login(self):
        user = User.objects.create_user(username='testuser', password='testpassword')

        url = reverse('users:api-login')
        data = {
            'username': 'testuser',
            'password': 'testpassword'
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'User logged in')
        self.assertEqual(authenticate(username='testuser', password='testpassword'), user)
