from django.urls import (
    include, 
    path,
)
from users.views import *

app_name = 'users'

urlpatterns = [
    path('api/users/register/', UserRegistrationAPIView.as_view(), name='api-register'),
    path('api/users/login/', UserLoginAPIView.as_view(), name='api-login'),
    path('api/users/logout/', UserLogoutAPIView.as_view(), name='api-logout'),
    path('api-auth/', include('rest_framework.urls')),
]
