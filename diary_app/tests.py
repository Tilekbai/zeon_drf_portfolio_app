from django.test import (
    TestCase, 
    RequestFactory,
)
from .models import (
    Entry,
)
from rest_framework import status
from django.contrib.auth import get_user_model
from rest_framework.test import force_authenticate
from django.conf import settings
from .views import *
from django.core.exceptions import ObjectDoesNotExist


User = get_user_model()

class EntryListViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = EntryListView.as_view()
        self.entries = Entry.objects.create(title='Test title', content='Test content')

    def test_entry_list_view(self):
        request = self.factory.get('api/diary/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['entries']), 1)

    def test_entry_list_view_empty(self):
        Entry.objects.all().delete()
        request = self.factory.get('api/diary/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['entries']), 0)

class EntryDetailViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = EntryDetailView.as_view()
        self.entry = Entry.objects.create(
            title='Entry 1',
            content='content',
        )

    def test_entry_detail_view(self):
        request = self.factory.get(f'api/diary/entry/{self.entry.pk}/')
        response = self.view(request, pk=self.entry.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['entry']['id'], self.entry.id)
        self.assertEqual(response.data['entry']['title'], self.entry.title)

    def test_entry_detail_view_not_found(self):
        request = self.factory.get('/api/diary/entry/999/')
        response = self.view(request, pk=999)
        self.assertEqual(response.status_code, 404)

class EntryCreateViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = EntryCreateView.as_view()

    def test_entry_create_view(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.post('api/diary/entry/add/', {
            'title': 'Entry 1',
            'content': 'content'
        }, format='json')

        force_authenticate(request, user=user)
        response = self.view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['title'], 'Entry 1')

    def test_entry_create_view_invalid_data(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.post('api/diary/entry/add/', {
            'title': '',
            'content': 'content'
        }, format='json')

        force_authenticate(request, user=user)
        response = self.view(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_entry_create_view_unauthenticated(self):
        request = self.factory.post('api/diary/entry/add/', {
            'title': 'Test Title',
            'content': 'content'
        }, format='json')

        response = self.view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class EntryUpdateViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = EntryUpdateView.as_view()
        self.entry = Entry.objects.create(title='Test Entry', content='Test content')

    def test_entry_update_view(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.put(f'/api/diary/entry/{self.entry.pk}/edit/', {
            'title': 'Updated title',
            'content': 'content',
        }, content_type='application/json')

        force_authenticate(request, user=user)
        response = self.view(request, pk=self.entry.pk)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], 'Updated title')
        self.assertEqual(response.data['content'], 'content')

class EntryDeleteViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = EntryDeleteView.as_view()
        self.entry = Entry.objects.create(
            title='Test title',
            content='Test content'
            )

    def test_entry_delete_view(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.delete(f'api/diary/entry/{self.entry.pk}/delete/')
        force_authenticate(request, user=user)
        response = self.view(request, pk=self.entry.pk)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Entry.objects.filter(pk=self.entry.pk).exists())

    def test_entry_delete_view_not_found(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.delete('api/diary/entry/{self.entry.pk}/delete/')
        force_authenticate(request, user=user)
        response = self.view(request, pk=999)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_entry_delete_view_notauthenticated(self):
        request = self.factory.delete('api/diary/entry/{self.entry.pk}/delete/')
        response = self.view(request, pk=999)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
