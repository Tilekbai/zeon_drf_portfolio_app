from django.urls import (
    path,
)
from .views import *


urlpatterns = [
    path('', EntryListView.as_view(), name='entry-list'),
    path('entry/<int:pk>/', EntryDetailView.as_view(), name='entry-detail'),
    path("entry/add/", EntryCreateView.as_view(), name="entry-add"),
    path("entry/<int:pk>/edit/", EntryUpdateView.as_view(), name="entry-update"),
    path("entry/<int:pk>/delete/", EntryDeleteView.as_view(), name="entry-delete"),
]