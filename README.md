# zeon_drf_portfolio_app


# Project Deployment Instructions


## Инструкция по развертыванию проекта

Склонируйте репозиторий из Gitlab на свой локальный компьютер с помощью команды:
git clone https://gitlab.com/Tilekbai/zeon_drf_portfolio_app.git


Перейдите в корневой каталог проекта.

Создайте виртуальное окружение, выполнив команду:

python3 -m venv venv


Активируйте виртуальное окружение, выполнив команду:
source venv/bin/activate

Создайте базу данных PostgresQL

Создайте файл - .env и передайте в нее настройки базы данных:

- POSTGRES_HOST=db
- POSTGRES_PORT=5432
- POSTGRES_NAME=***
- POSTGRES_USER=***
- POSTGRES_PASSWORD=***


Прокатите миграции: 
- python manage.py migrate


Создайте суперпользователя:
- python manage.py createsuperuser

Перейдите по адресу: localhost:8000/api/projects/