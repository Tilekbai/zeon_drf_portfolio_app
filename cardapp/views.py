import random
from django.shortcuts import (
    get_object_or_404,
)
from rest_framework.response import (
    Response,
)
from rest_framework import (
    generics,
) 
from rest_framework.views import (
    APIView,
    )
from rest_framework import (
    permissions,
    )
from .serializers import (
    CardSerializer,
    CardCheckSerializer,
)
from .models import (
    Card,
)

class CardListView(generics.ListAPIView):
    queryset = Card.objects.all()
    serializer_class = CardSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        context = {'cards': serializer.data}
        return Response(context)
        
class CardCreateView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, *args, **kwargs):
        serializer = CardSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)
        
class CardUpdateView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def put(self, request, pk, format=None):
        try:
            entry = Card.objects.get(pk=pk)
        except Card.DoesNotExist:
            return Response({'detail': 'Card not found.'}, status=404)

        serializer = CardSerializer(entry, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)
    
class BoxView(APIView):
    queryset = Card.objects.all()
        
    def get(self, request, box_num, format=None):
        queryset = Card.objects.filter(box=box_num)
        serializer = CardSerializer(queryset, many=True)
        check_card = random.choice(queryset) if queryset else None
        check_card_serializer = CardSerializer(check_card) if check_card else None
        context = {
            'box_number': box_num,
            'check_card': check_card_serializer.data if check_card_serializer else None,
            'cards': serializer.data,
        }
        return Response(context)
    
    def post(self, request, box_num, format=None):
        serializer = CardCheckSerializer(data=request.data)
        if serializer.is_valid():
            card_id = serializer.validated_data["card_id"]
            solved = serializer.validated_data["solved"]
            card = get_object_or_404(Card, id=card_id)
            card.move(solved)
            if card.box > box_num:
                message = "Successfully! Card in the next box!"
                card_serializer = CardSerializer(card)
                return Response({'message': message, 'card': card_serializer.data})
        return Response(serializer.errors, status=400)
