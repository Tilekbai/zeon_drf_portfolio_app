from rest_framework import (
    serializers,
)
from .models import (
    Card,
)


class CardSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    question = serializers.CharField(max_length=100)
    answer = serializers.CharField(max_length=100)
    box = serializers.IntegerField(required=False)
    created_at = serializers.DateTimeField(read_only=True)

    def create(self, validated_data):
        return Card.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.question = validated_data.get('question', instance.question)
        instance.answer = validated_data.get('answer', instance.answer)
        instance.box = validated_data.get('box', instance.box)
        instance.save()
        return instance

class CardCheckSerializer(serializers.Serializer):
    card_id = serializers.IntegerField()
    solved = serializers.BooleanField()
    