from django.test import (
    TestCase, 
    RequestFactory,
)
from .models import (
    Card,
)
from rest_framework import status
from django.contrib.auth import get_user_model
from rest_framework.test import force_authenticate
from django.conf import settings
from .views import *


User = get_user_model()

class CardListViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = CardListView.as_view()
        self.card = Card.objects.create(
            question='Test question', 
            answer='Test answer',
            )

    def test_card_list_view(self):
        request = self.factory.get(f'api/flashcards/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['cards']), 1)

    def test_card_list_view_empty(self):
        Card.objects.all().delete()
        request = self.factory.get(f'api/flashcards/')
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['cards']), 0)

class CardCreateViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = CardCreateView.as_view()

    def test_card_create_view(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.post('api/flashcards/new/', {
            'question': 'Test question',
            'answer': 'Test answer'
        }, format='json')

        force_authenticate(request, user=user)
        response = self.view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['question'], 'Test question')

    def test_card_create_view_invalid_data(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.post('api/flashcards/new/', {
            'question': '',
            'answer': 'Test answer'
        }, format='json')

        force_authenticate(request, user=user)
        response = self.view(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_card_create_view_unauthenticated(self):
        request = self.factory.post('api/flashcards/new/', {
            'question': 'Test question',
            'answer': 'Test answer'
        }, format='json')

        response = self.view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class CardUpdateViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = CardUpdateView.as_view()
        self.card = Card.objects.create(question='Test question', answer='Test answer')

    def test_card_update_view(self):
        user = User.objects.create_user(username='testuser', password='testpassword')
        request = self.factory.put(f'/api/flashcards/edit/{self.card.pk}/', {
            'question': 'Updated',
            'answer': 'Updated',
        }, content_type='application/json')

        force_authenticate(request, user=user)
        response = self.view(request, pk=self.card.pk)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['question'], 'Updated')
        self.assertEqual(response.data['answer'], 'Updated')

class BoxViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        settings.DATABASES['default'] = settings.DATABASES['test']
        self.factory = RequestFactory()
        self.view = BoxView.as_view()
        self.cards = [
            (Card.objects.create(
            question='Test question 1', 
            answer='Test answer 1',
            box=1
            )),
            (Card.objects.create(
            question='Test question 2', 
            answer='Test answer 2',
            box=2
            )),
        ]

    def test_box_view_get(self):
        request = self.factory.get(f'api/flashcards/box/{self.cards[0].box}/')
        response = self.view(request, box_num=self.cards[0].box)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['cards']), 1)

    def test_box_view_get_empty(self):
        Card.objects.all().delete()
        request = self.factory.get(f'api/flashcards/box/{self.cards[0].box}/')
        response = self.view(request, box_num=self.cards[0].box)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['cards']), 0)
